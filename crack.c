#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char* word;
    char* hash;
};

// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    *size = 0;
    
    if (filename == NULL) return NULL;
    
    // get total number of lines in the file
    int lineCount = 0;
    char line[1000];
    FILE* f = fopen(filename, "r");
    while (fgets(line, 1000, f) != NULL)
    {
        lineCount ++;
    }
    *size = lineCount;
    fclose(f);

    // allocate memory and get contents
    
    // each element of this array, e.g. entries[i] is a pointer that points to a line of string
    struct entry *entries = malloc(lineCount * sizeof(struct entry));
    FILE* file = fopen(filename, "r");
    int n = 0;
    while (fgets(line, 1000, file) != NULL)
    {
        int lineLength = strlen(line);
        if (line[lineLength - 1] == '\n')
        {
            line[lineLength - 1] = '\0';
        }
        char* word = malloc(strlen(line) * sizeof(char) + 1);
        strcpy(word, line);
        char *hash = md5(word, strlen(word));
        
        entries[n].word = word;
        entries[n].hash = hash;
        
        n++;
    }
    fclose(file);
    
    return entries;
}

int entryComp(const void *a, const void *b)
{
    struct entry *entryA = (struct entry *)a;
    struct entry *entryB = (struct entry *)b;
    return strcmp(entryA->hash, entryB->hash);
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of entry structures
    
    int dLines = 0;
    struct entry *dict = read_dictionary(argv[2], &dLines);
    
    // Sort the hashed dictionary using qsort
    
    qsort(dict, dLines, sizeof(struct entry), entryComp);
    
    // Open the hash file for reading.
    
    FILE* hashes = fopen(argv[1], "r");

    // For each hash, search for it in the dictionary using binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    
    char hash[1000];
    
    while (fgets(hash, 1000, hashes) != NULL)
    {
        int hashLength = strlen(hash);
        if (hash[hashLength - 1] == '\n')
        {
            hash[hashLength - 1] = '\0';
        }
        
        // create the target entry
        
        struct entry target = {NULL, hash};
        
        struct entry *found = bsearch(&target, dict, dLines, sizeof(struct entry), entryComp);
        
        printf("Hash = %s, ", hash);
        
        if (found == NULL)
        {
            printf("no password found\n");
        }
        else
        {
            printf("password = %s\n", found->word);
        }
    }
    
    fclose(hashes);
    
    // free allocated memory
    
    for (int i = 0; i < dLines; i++)
    {
        if (dict[i].word != NULL)
        {
            free(dict[i].word);
        }
        
        if (dict[i].hash != NULL)
        {
            free(dict[i].hash);
        }
    }
    
    if (dict != NULL)
    {
        free(dict);
    }
    
    return 0;
}
